export interface Team {
  id: string,
  name: string;
  fullname: string;
}