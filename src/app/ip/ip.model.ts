import { Team } from "../team/team.model";

export class Ip {
  id: string;
  no: string;
  status: boolean;
  ipaddress: string;
  comno: string;
  name: string;
  teams: Team[]
  team: string;
  remark: string;
  edit: string;
}

export interface IpDisplay {
  id: { header: string; display: boolean };
  no: { header: string; display: boolean };
  status: { header: string; display: boolean };
  ipaddress: { header: string; display: boolean };
  comno: { header: string; display: boolean };
  name: { header: string; display: boolean };
  team: { header: string; display: boolean };
  remark: { header: string; display: boolean };
  edit: { header: string; display: boolean };
}
