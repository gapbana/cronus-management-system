import { Component, Inject } from "@angular/core";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'confirm-delete-dialog',
    templateUrl: 'confirm-delete-dialog.html'
  })
  export class ConfirmDeleteDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmDeleteDialogComponent>) { }

    onCloseCancel(): void {
        this.dialogRef.close(false);
    }

    onCloseConfirm(): void {
        this.dialogRef.close(true);
    }
}