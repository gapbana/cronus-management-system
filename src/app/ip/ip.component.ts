import { Component, OnInit, OnDestroy, ViewChild , OnChanges, DoCheck, AfterContentInit } from "@angular/core";
import { AuthenticationService } from '../authentication.service';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from "@angular/material";
import { Ip, IpDisplay } from "./ip.model";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { Subscription } from "rxjs";
import { IpsService } from "../ip/ip.service"
import { ErrorDialogComponentip } from "../ip/error-dialog/error-dialog.component";
import { ConfirmDeleteDialogComponent } from "../member/member-list/confirm-delete-dialog.component";
import { and } from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-ip',
  templateUrl: './ip.component.html',
  styleUrls: ['./ip.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})

export class IpComponent implements OnInit   {
  row = 0;
  ips: Ip[] = [];
  isLoading = false;
  private ipsSub: Subscription;
  allColumns: string[];
  displayedColumns: string[];
  displayedHeaders: string[];
  displayedEnable: boolean[];
  field: IpDisplay;
  dataSource: MatTableDataSource<Ip>;

  constructor(public ipsService: IpsService, public dialog: MatDialog) { }

  filterColumnOfCheckbox() {
    return this.allColumns
      .filter(object => object != "id")
      .filter(object => object != "action");
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.row = 0;
    let i = 1;
    this.field = {
      id: { header: "Id.", display: false },
      no: { header: "No.", display: true },
      status: { header: "Status", display: true },
      ipaddress: { header: "IP Address", display: true },
      comno: { header: "Com. No.", display: true },
      name: { header: "Name", display: true },
      team: { header: "Team", display: true },
      remark: { header: "Remark", display: true },
      edit: { header: "Edit", display: true }

    };
    this.allColumns = [];
    this.displayedColumns = [];
    this.displayedHeaders = [];
    this.displayedEnable = [];

    for (var key of Object.keys(this.field)) {
      this.allColumns.push(key);
      this.displayedHeaders[key] = this.field[key].header;
      this.displayedEnable[key] = this.field[key].display;
      if (this.displayedEnable[key]) {
        this.displayedColumns.push(key);
      }
    }
    this.refreshIp()
    
  }

  ngDoCheck() {
    this.row = 0;
  }

  ngOnChanges(changes: any) {
      this.row = 0;
  }

  resetRow(){
    this.row = 0;
  }

  getRow(){
    this.row++;
    return this.row;
  }

  getStatus ( name: string , comno : string) {
     
    if (name == ''){
      if (comno == '') {
        return "Available"
      }
    } else if (name != '') { 
      return "InUse"
    }
    else  { 
      return "InUse"
    }

  }

  getClass (label : string) {
    if (label == "Available") {
      return "TypeFrame ColorAva"
    } else {
      return "TypeFrame ColorInUse"
    }
  }


  refreshIp() {
    this.isLoading = true;
    this.ipsSub = this.ipsService
      .getIpUpdateListener()
      .subscribe((ips: Ip[]) => {
        this.isLoading = false;
        this.ips = ips;
        this.dataSource = new MatTableDataSource<Ip>(this.ips);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  showAllColumns(visible) {
    for (var key of this.filterColumnOfCheckbox()) {
      this.displayedEnable[key] = visible;
    }
    if (!visible) {
      for (var key of Object.keys(this.field)) {
        this.displayedEnable[key] = this.field[key].display;
      }
    }
    this.showColumns(event);
  }

  showColumns(event) {
    this.displayedColumns = [];
    for (var key of this.allColumns) {
      if (this.displayedEnable[key]) {
        this.displayedColumns.push(key);
      }
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(id: string) {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ipsService.deleteIp(id).subscribe(
          res => {
            this.refreshIp()
          },
          err => {
            this.dialog.open(ErrorDialogComponentip, {
              width: '400px',
              data: err.message
            });
          }
        );
      }
    });
  }
}
