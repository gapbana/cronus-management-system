import { Component, OnInit } from "@angular/core";
import { FormControl, NgForm } from "@angular/forms";
import { IpsService } from "../ip.service";
import { Ip } from "../ip.model";
import { ActivatedRoute, ParamMap } from "@angular/router";
import {Router} from "@angular/router";
import { ErrorDialogComponentip } from "../error-dialog/error-dialog.component";
import { MatDialog } from "@angular/material";
import { Team } from "../../team/team.model";
import { TeamsService } from "../../team/team.service";

import { flatMap, map, tap } from 'rxjs/operators';


@Component({
  selector: "app-ip-create",
  templateUrl: "./ip-create.component.html",
  styleUrls: ["./ip-create.component.css"]
})
export class IpCreateComponent  {
  ip = new Ip();
  isLoading = false;
  private mode = "create";

  companyCtrl = new FormControl();
  filteredCompanies: string[];
  companies: string[] = [];
  teams: Team[];

  constructor(
    public ipsService: IpsService,
    public teamsService: TeamsService,
    public route: ActivatedRoute,
    public router: Router,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    
    // Load teams
    this.teamsService.getTeams()
      .subscribe((teams: Team[]) => {
        this.teams = teams;
      });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      // Update mode
      if (paramMap.has("id")) {
        this.mode = "edit";
        this.ip.id = paramMap.get("id");
        this.isLoading = true;
        this.ipsService.getIp(this.ip.id).subscribe(ipData => {
          this.isLoading = false;
          this.ip = ipData;
        });
      } else {
        // Create mode
        this.mode = "create";
      }
    });


  }

  companyFilter(value) {
    return value ? this._filterCompanies(value) : this.companies.slice();
  }

  compareTeam(t1: Team, t2: Team): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  private _filterCompanies(value: string): string[] {
    var filterValue = value.toLocaleLowerCase();

    return this.companies.filter(
      company => company.toLocaleLowerCase().indexOf(filterValue) == 0
    );
  }


  onSaveIp(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === "create") {
      this.ipsService.addIp(this.ip).subscribe(
        res => {
          // form.resetForm();
          this.isLoading = false;
          this.router.navigate(["/ip"]);
        },
        err => {
          // Display error popup 
          this.isLoading = false;
          this.dialog.open(ErrorDialogComponentip, {
            width: '500px',
            data: err.error.message
          });
          console.log(err);
        }
      )
    } else {
      this.ipsService.updateIp(this.ip).subscribe(
        res => {
          // form.resetForm();
          this.isLoading = false;
          this.router.navigate(["/ip"]);
        },
        err => {
          // Display error popup 
          this.isLoading = false;
          this.dialog.open(ErrorDialogComponentip, {
            width: '500px',
            data: err.error.message
          });
          console.log(err);
        }
      );
    }
  }
}
