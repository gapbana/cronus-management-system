import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorDialogComponentip } from './error-dialog.component';

describe('ErrorDialogComponent', () => {
  let component: ErrorDialogComponentip;
  let fixture: ComponentFixture<ErrorDialogComponentip>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorDialogComponentip ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorDialogComponentip);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
