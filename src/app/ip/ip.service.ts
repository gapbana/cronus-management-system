import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Ip } from "./ip.model";

@Injectable({ providedIn: "root" })
export class IpsService {
  constructor(private http: HttpClient) {}

  getIpUpdateListener() {
    return this.http.get<Ip[]>("http://localhost:3000/api/ip").pipe();
  }

  getIp(id: string) {
    return this.http.get<Ip>("http://localhost:3000/api/ip/" + id);
  }

  addIp(ip: Ip) {
    return this.http.post("http://localhost:3000/api/ip", ip);
  }

  updateIp(ip: Ip) {
    return this.http.put("http://localhost:3000/api/ip/" + ip.id, ip)
  }

  deleteIp(id: string) {
    return this.http.delete("http://localhost:3000/api/ip/" + id)
  }

 
}
