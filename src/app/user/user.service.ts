import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";

import { User } from "./user.model";

@Injectable({ providedIn: "root" })

export class UserService {
    constructor(private http: HttpClient, private router: Router) {}

    login(username: string, password: string) {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
          };

        this.http
        .post("http://localhost:3000/api/user/login", { username, password }, httpOptions)
        .subscribe(resp => {
            console.log(resp);
        });
    }
}