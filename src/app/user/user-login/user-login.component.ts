import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from "@angular/forms";

import { UserService } from "../user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserComponent implements OnInit {

  constructor(
    public userService: UserService
  ) { }

  ngOnInit() {
  }

  onLogin(form: NgForm) {
    if (form.invalid) {
      console.log('form.invalid');
      return;
    }

    console.log('login');

    this.userService.login(form.value.username, form.value.password);
  }
}
