import { Component, OnInit } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../../authentication.service';
import { Router } from '@angular/router';
import { FormControl, NgForm } from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  credentials: TokenPayload = {
    username: '',
    password: ''
  };

  constructor(private auth: AuthenticationService, private router: Router) {}

  ngOnInit() {
  }

  register(form: NgForm) {
    if (form.invalid) {
      console.log('form.invalid');
      return;
    }

    this.credentials.username = form.value.username;
    this.credentials.password = form.value.password;

    this.auth.register(this.credentials).subscribe(() => {
      this.router.navigateByUrl('/member');
    }, (err) => {
      console.error(err);
    });
  }
}
