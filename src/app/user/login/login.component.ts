import { Component, OnInit } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../../authentication.service';
import { Router } from '@angular/router';
import { FormControl, NgForm } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: TokenPayload = {
    username: '',
    password: ''
  };

  constructor(private auth: AuthenticationService, private router: Router) {}

  ngOnInit() {
  }

  login(form: NgForm) {
    if (form.invalid) {
      console.log('form.invalid');
      return;
    }

    this.credentials.username = form.value.username;
    this.credentials.password = form.value.password;

    this.auth.login(this.credentials).subscribe(() => {
      this.router.navigateByUrl('/member');
    }, (err) => {
      console.log('login fail.');
      console.error(err);
    });
  }
}
