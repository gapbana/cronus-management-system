import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatIconModule,
  MatMenuModule,
  MatListModule,
  MatDividerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  DateAdapter,
  MAT_DATE_FORMATS
} from "@angular/material";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { MemberCreateComponent } from "./member/member-create/member-create.component";
import { MemberListComponent } from "./member/member-list/member-list.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppDateAdapter, APP_DATE_FORMATS } from "./date.adapter";
import { ConfirmDeleteDialogComponent } from "./member/member-list/confirm-delete-dialog.component";
import { ErrorDialogComponent } from './member/error-dialog/error-dialog.component';
import { UserComponent } from './user/user-login/user-login.component';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { IpComponent } from './ip/ip.component'
import { IpCreateComponent } from './ip/ip-create/ip-create.component'
import { ErrorDialogComponentip } from './ip/error-dialog/error-dialog.component';

@NgModule({
  entryComponents: [
    ConfirmDeleteDialogComponent,
    ErrorDialogComponent,
    ErrorDialogComponentip
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    MemberCreateComponent,
    MemberListComponent,
    ConfirmDeleteDialogComponent,
    ErrorDialogComponent,
    ErrorDialogComponentip,
    UserComponent,
    RegisterComponent,
    LoginComponent,
    IpComponent,
    IpCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("en-US");
  }
}
