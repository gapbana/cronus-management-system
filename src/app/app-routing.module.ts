import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MemberListComponent } from "./member/member-list/member-list.component";
import { MemberCreateComponent } from "./member/member-create/member-create.component";
//import { UserComponent } from "./user/user-login/user-login.component";
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { IpComponent } from './ip/ip.component'  //add by gap
import { IpCreateComponent } from './ip/ip-create/ip-create.component'

import {AuthGuardService} from './auth-guard.service';
const routes: Routes = [
    { path: 'member', component: MemberListComponent, canActivate: [AuthGuardService] },
    { path: 'member/create', component: MemberCreateComponent, canActivate: [AuthGuardService] },
    { path: 'member/edit/:id', component: MemberCreateComponent, canActivate: [AuthGuardService] },
    { path: 'login', component: LoginComponent},
    { path: 'register', component: RegisterComponent },
    { path: '', component: LoginComponent},
    { path: 'ip', component: IpComponent},  //bygap
    //{ path: '', component: LoginComponent},
    { path: 'ip/create' , component: IpCreateComponent, canActivate: [AuthGuardService] },
    { path: 'ip/edit/:id', component: IpCreateComponent, canActivate: [AuthGuardService] }
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
