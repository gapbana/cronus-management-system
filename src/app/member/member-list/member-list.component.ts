import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Member, MemberDisplay } from "../member.model";
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from "@angular/material";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { MembersService } from "../member.service";
import { Subscription } from "rxjs";
import { ConfirmDeleteDialogComponent } from "./confirm-delete-dialog.component";
import { ErrorDialogComponent } from "../error-dialog/error-dialog.component";

@Component({
  selector: "app-member-list",
  templateUrl: "./member-list.component.html",
  styleUrls: ["./member-list.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class MemberListComponent implements OnInit, OnDestroy {
  members: Member[] = [];
  isLoading = false;
  private membersSub: Subscription;

  allColumns: string[];
  displayedColumns: string[];
  displayedHeaders: string[];
  displayedEnable: boolean[];
  field: MemberDisplay;
  expandedElement: any;
  dataSource: MatTableDataSource<Member>;

  constructor(public membersService: MembersService, public dialog: MatDialog) {}

  filterColumnOfCheckbox() {
    return this.allColumns
      .filter(object => object != "id")
      .filter(object => object != "action");
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.field = {
      id: { header: "", display: false },
      nameen: { header: "Name (EN)", display: true },
      surnameen: { header: "Surname (EN)", display: true },
      nicknameen: { header: "Nickname (EN)", display: true },
      nameth: { header: "Name (TH)", display: false },
      surnameth: { header: "Surname (TH)", display: false },
      nicknameth: { header: "Nickname (TH)", display: false },
      tel: { header: "Tel.", display: false },
      email: { header: "Email", display: false },
      dateofbirth: { header: "Date of birth", display: false },
      username: { header: "Username", display: false },
      workemail: { header: "Work Email", display: false },
      company: { header: "Company", display: true },
      cardid: { header: "Card ID", display: false },
      team: { header: "Team", display: true },
      action: { header: "", display: true }
    };
    this.allColumns = [];
    this.displayedColumns = [];
    this.displayedHeaders = [];
    this.displayedEnable = [];
    for (var key of Object.keys(this.field)) {
      this.allColumns.push(key);
      this.displayedHeaders[key] = this.field[key].header;
      this.displayedEnable[key] = this.field[key].display;
      if (this.displayedEnable[key]) {
        this.displayedColumns.push(key);
      }
    }
    this.refreshMember()
  }

  refreshMember() {
    this.isLoading = true;
    this.membersSub = this.membersService
      .getMemberUpdateListener()
      .subscribe((members: Member[]) => {
        this.isLoading = false;
        this.members = members;
        this.dataSource = new MatTableDataSource<Member>(this.members);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  ngOnDestroy() {
    this.membersSub.unsubscribe();
  }

  showAllColumns(visible) {
    for (var key of this.filterColumnOfCheckbox()) {
      this.displayedEnable[key] = visible;
    }
    if (!visible) {
      for (var key of Object.keys(this.field)) {
        this.displayedEnable[key] = this.field[key].display;
      }
    }
    this.showColumns(event);
  }

  showColumns(event) {
    this.displayedColumns = [];
    for (var key of this.allColumns) {
      if (this.displayedEnable[key]) {
        this.displayedColumns.push(key);
      }
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(id: string) {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.membersService.deleteMember(id).subscribe(
          res => {
            this.refreshMember()
          },
          err => {
            this.dialog.open(ErrorDialogComponent, {
              width: '400px',
              data: err.message
            });
          }
        );
      }
    });
  }
}
