import { Team } from "../team/team.model";

export class Member {
  id: string;
  nameen: string;
  surnameen: string;
  nicknameen: string;
  nameth: string;
  surnameth: string;
  nicknameth: string;
  tel: string;
  email: string;
  dateofbirth: Date;
  username: string;
  workemail: string;
  company: string;
  cardid: string;
  teams: Team[];
  team: string;
}

export interface MemberDisplay {
  id: { header: string; display: boolean };
  nameen: { header: string; display: boolean };
  surnameen: { header: string; display: boolean };
  nicknameen: { header: string; display: boolean };
  nameth: { header: string; display: boolean };
  surnameth: { header: string; display: boolean };
  nicknameth: { header: string; display: boolean };
  tel: { header: string; display: boolean };
  email: { header: string; display: boolean };
  dateofbirth: { header: string; display: boolean };
  username: { header: string; display: boolean };
  workemail: { header: string; display: boolean };
  company: { header: string; display: boolean };
  cardid: { header: string; display: boolean };
  team: { header: string; display: boolean };
  action: { header: string; display: boolean };
}
