import { Component, OnInit } from "@angular/core";
import { FormControl, NgForm } from "@angular/forms";
import { MembersService } from "../member.service";
import { Member } from "../member.model";
import { ActivatedRoute, ParamMap } from "@angular/router";
import {Router} from "@angular/router";
import { ErrorDialogComponent } from "../error-dialog/error-dialog.component";
import { MatDialog } from "@angular/material";
import { Team } from "../../team/team.model";
import { TeamsService } from "../../team/team.service";
import { flatMap, map, tap } from 'rxjs/operators';


@Component({
  selector: "app-member-create",
  templateUrl: "./member-create.component.html",
  styleUrls: ["./member-create.component.css"]
})
export class MemberCreateComponent implements OnInit {
  member = new Member();
  isLoading = false;
  private mode = "create";

  companyCtrl = new FormControl();
  filteredCompanies: string[];
  companies: string[] = [];
  teams: Team[];

  constructor(
    public membersService: MembersService,
    public teamsService: TeamsService,
    public route: ActivatedRoute,
    public router: Router,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    // Load teams
    this.teamsService.getTeams()
      .subscribe((teams: Team[]) => {
        this.teams = teams;
      });
    // Load companies
    this.membersService.getMemberCompanies()
      .subscribe((companies: string[]) => {
        this.companies = companies;
      })
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      // Update mode
      if (paramMap.has("id")) {
        this.mode = "edit";
        this.member.id = paramMap.get("id");
        this.isLoading = true;
        this.membersService.getMember(this.member.id).subscribe(memberData => {
          this.isLoading = false;
          this.member = memberData;
        });
      } else {
        // Create mode
        this.mode = "create";
      }
    });
  }

  companyFilter(value) {
    return value ? this._filterCompanies(value) : this.companies.slice();
  }

  private _filterCompanies(value: string): string[] {
    var filterValue = value.toLocaleLowerCase();

    return this.companies.filter(
      company => company.toLocaleLowerCase().indexOf(filterValue) == 0
    );
  }

  compareTeam(t1: Team, t2: Team): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  onSaveMember(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === "create") {
      this.membersService.addMember(this.member).subscribe(
        res => {
          // form.resetForm();
          this.isLoading = false;
          this.router.navigate(["/member"]);
        },
        err => {
          // Display error popup 
          this.isLoading = false;
          this.dialog.open(ErrorDialogComponent, {
            width: '500px',
            data: err.error.message
          });
          console.log(err);
        }
      )
    } else {
      this.membersService.updateMember(this.member).subscribe(
        res => {
          // form.resetForm();
          this.isLoading = false;
          this.router.navigate(["/member"]);
        },
        err => {
          // Display error popup 
          this.isLoading = false;
          this.dialog.open(ErrorDialogComponent, {
            width: '500px',
            data: err.error.message
          });
          console.log(err);
        }
      );
    }
  }
}
