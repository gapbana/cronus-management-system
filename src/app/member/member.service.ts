import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Member } from "./member.model";

@Injectable({ providedIn: "root" })
export class MembersService {
  constructor(private http: HttpClient) {}

  getMemberUpdateListener() {
    return this.http.get<Member[]>("http://localhost:3000/api/members").pipe();
  }

  getMember(id: string) {
    return this.http.get<Member>("http://localhost:3000/api/members/" + id);
  }

  addMember(member: Member) {
    return this.http.post("http://localhost:3000/api/members", member);
  }

  updateMember(member: Member) {
    return this.http.put("http://localhost:3000/api/members/" + member.id, member)
  }

  deleteMember(id: string) {
    return this.http.delete("http://localhost:3000/api/members/" + id)
  }

  getMemberCompanies() {
    return this.http.get<string[]>("http://localhost:3000/api/members/companies");
  }
}
