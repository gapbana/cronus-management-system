const express = require('express');
const Member = require('../models/member');
const router = express.Router();

router.get('', (req, res, next) => {
  Member.find().then(members => {
    res.json(members);
  });
});

router.get('/companies', (req, res, next) => {
  Member.distinct('company').then(result => {
    res.json(result);
  }).catch(function(e) {
    console.log(e);
  })
});

router.get('/:id', (req, res, next) => {
  Member.findById(req.params.id).then(member => {
    if (member) {
      res.status(200).json(member);
    } else {
      res.status(404).json({
        message: "Member not found!"
      });
    }
  });
});

router.post("", (req, res, next) => {
  // console.log(req.body)
  const member = new Member(req.body)
  console.log(member)
  member.save().then(createdMember => {
    res.status(201).json(member);
  }).catch(e => {
    res.status(400).json(e);
  })
});

router.put("/:id", (req, res, next) => {
  Member.updateOne({
    _id: req.params.id
  }, req.body).then(result => {
    res.status(200).json({
      message: "Update successful!"
    });
  }).catch(e => {
    res.status(400).json(e);
  })
});

router.delete("/:id", (req, res, next) => {
  Member.deleteOne({
    _id: req.params.id
  }).then(result => {
    console.log(result);
    res.status(200).json({
      message: "Member deleted!"
    });
  }).catch(e => {
    res.status(400).json(e);
  });
});

module.exports = router;
