const express = require('express');
const Team = require('../models/team').teamModel;
const router = express.Router();

router.get('', (req, res, next) => {
  Team.find().then(teams => {
    res.json(teams);
  });
});

router.get('/:id', (req, res, next) => {
  Team.findById(req.params.id).then(team => {
    if (team) {
      res.json(team);
    } else {
      res.status(404).json({
        message: "Team not found!"
      });
    }
  });
});

module.exports = router;