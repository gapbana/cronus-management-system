const express = require('express');
const Ip = require('../models/Ip');
const router = express.Router();

router.get('', (req, res, next) => {
  Ip.find().then(Ips => {
    res.json(Ips);
  });
});

/* router.get('/companies', (req, res, next) => {
  Ip.distinct('company').then(result => {
    res.json(result);
  }).catch(function(e) {
    console.log(e);
  })
}); */

router.get('/:id', (req, res, next) => {
  Ip.findById(req.params.id).then(Ip => {
    if (Ip) {
      res.status(200).json(Ip);
    } else {
      res.status(404).json({
        message: "Ip not found!"
      });
    }
  });
});

router.post("", (req, res, next) => {
  const ip = new Ip(req.body)
  console.log(ip)
  ip.save().then(createdIp => {
    res.status(201).json(ip);
  }).catch(e => {
    res.status(400).json(e);
  })
});

router.put("/:id", (req, res, next) => {
  Ip.updateOne({
    _id: req.params.id
  }, req.body).then(result => {
    res.status(200).json({
      message: "Update successful!"
    });
  }).catch(e => {
    res.status(400).json(e);
  })
});

router.delete("/:id", (req, res, next) => {
  Ip.deleteOne({
    _id: req.params.id
  }).then(result => {
    console.log(result);
    res.status(200).json({
      message: "Ip deleted!"
    });
  }).catch(e => {
    res.status(400).json(e);
  });
});

module.exports = router;
