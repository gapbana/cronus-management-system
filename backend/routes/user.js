const express = require('express');
const router = express.Router();

const passport = require('passport');

const User = require('../models/user');

const jwt = require('express-jwt');
const auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

router.post('/login', (req, res, next) => {
    /*
    console.log(req.body.username + ':' + req.body.password);

    res.status(200).json({
        IsOK: true
    });
    */
    console.log('login:' + req.body.username + ':' + req.body.password);
    /*
    var user = new User();

    user.username = req.body.username;
    user.password = req.body.password;
    */
    passport.authenticate('local', function(err, user, info) {
        //console.log(user);

        var token;

        // If Passport throws/catches an error
        if (err) {
            res.status(404).json(err);
            return;
        }

        // If a user is found
        if(user) {
            token = user.generateJwt();
            res.status(200);
            res.json({
                "token" : token
            });
        } else {
            // If user is not found
            res.status(401).json(info);
        } 
    })(req, res);
});

router.post('/register', (req, res, next) => {
    console.log('register:' + req.body.username + ':' + req.body.password);

    var user = new User();

    user.username = req.body.username;
    user.setPassword(req.body.password);

    user.save(function(err) {
        if(err) {
            res.status(401).json({
                "IsOK": false,
                "message": err

            });

            return;
        }

        var token;
        token = user.generateJwt();
        res.status(200);
        res.json({
          "token" : token
        });
    });
});

router.get('/getdata', auth, (req, res, next) => {
    res.status(200).send({
        IsOK: true
    });

});

module.exports = router;