const mongoose = require('mongoose');
const teamSchema = require('./team').teamSchema;
const memberSchema = mongoose.Schema({
  nameen: {
    type: String,
    required: [true, 'Name (EN) is required']
  },
  surnameen: {
    type: String,
    required: [true, 'Surname (EN) is required']
  },
  nicknameen: {
    type: String,
    required: [true, 'Nickname (EN) is required']
  },
  nameth: {
    type: String,
    required: [true, 'Name (TH) is required']
  },
  surnameth: {
    type: String,
    required: [true, 'Surname (TH) is required']
  },
  nicknameth: {
    type: String,
    required: [true, 'Nickname (TH) is required']
  },
  tel: {
    type: String,
    required: [true, 'Tel is required']
  },
  email: {
    type: String,
    required: [true, 'Email is required']
  },
  dateofbirth: {
    type: Date,
    required: [true, 'Date of birth is required']
  },
  username: {
    type: String,
    required: [true, 'Username is required']
  },
  workemail: {
    type: String
  },
  company: {
    type: String,
    required: [true, 'Company is required']
  },
  cardid: {
    type: String
  },
  teams: [
    {
      type: teamSchema,
      ref: 'Team'
    }
  ]
});
memberSchema.set('toJSON', {
  virtuals: true
});
memberSchema.virtual('team').get(function () {
  if (this.teams.length == 0) {
    return null;
  }
  return Array.prototype.map.call(this.teams, s => s.name).toString()
});

module.exports = mongoose.model('Member', memberSchema, 'Member');
