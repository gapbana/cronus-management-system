const mongoose = require('mongoose');
const teamSchema = require('./team').teamSchema;
const ipSchema = mongoose.Schema({
  no: {
    type: Number,
    required: false,
  },
  status: {
    type: String,
    required: false 
  },
  ipaddress: {
    type: String,
    required: [true, 'IP Address is required'],
    validate: {
      validator: function(v) {
        return /^([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})$/.test(v)
      },
      message: '{VALUE} is not a valid format ip address!'
    }

  },
  comno: {
    type: String,
    required: false,
    validate: {
      validator: function(v) {
        return /^([A-Z]{6,6})[_]([0-9]{1,2})$/.test(v)
      },
      message: '{VALUE} is not a valid format Com Number!'
    }
  },
  name: {
    type: String,
    required: false
  },
  teams: [
    {
      type: teamSchema,
      ref: 'Team'
    }
  ],
  remark: {
    type: String,
    required: false
  }
});
ipSchema.set('toJSON', {
  virtuals: true
});
ipSchema.virtual('team').get(function () {
  if (this.teams.length == 0) {
    return null;
  }
  return Array.prototype.map.call(this.teams, s => s.name).toString()
});

module.exports = mongoose.model('Ip', ipSchema, 'Ip');
