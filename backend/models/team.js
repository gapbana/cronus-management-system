const mongoose = require('mongoose');
const teamSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  fullname: {
    type: String
  }
});
teamSchema.set('toJSON', {
  virtuals: true
});
const teamModel = mongoose.model('Team', teamSchema, 'Team');
module.exports = {
  teamSchema: teamSchema,
  teamModel: teamModel
};