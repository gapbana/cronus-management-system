const mongoose = require('mongoose');
const crypto = require('crypto');

const jwt = require('jsonwebtoken');

const userSchema = mongoose.Schema({
    username: {
        type: String,
        require: true,
        validate: {
            validator: function (v) {
                return /[a-zA-Z0-9]/.test(v);
            },
            message: '{VALUE} is not a valid username!'
        },
        unique: true
    },

    password: {
        type: String,
        require: true
    },
    hash: String,
    salt: String,
    type: String   //fillter admin or user
});

userSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.validPassword = function (password) {
    //console.log('validPassword:' + password);

    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function () {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET"); // DO NOT KEEP YOUR SECRET IN THE CODE!

    /*
    Note: It’s important that your secret is kept safe: only the originating server should know what it is. 
    It’s best practice to set the secret as an environment variable, and not have it in the source code, 
    especially if your code is stored in version control somewhere.
    */
};

module.exports = mongoose.model('User', userSchema, 'User');