const express = require('express');
const bodyParser = require("body-parser");
const mongoose = require('mongoose');
const passport = require('passport');

const membersRoutes = require('./routes/members');
const teamsRoute = require('./routes/team');
const userRoutes = require('./routes/user');
const ipRoutes = require('./routes/ip');

const app = express();

const mongooseOptions = {
  dbName: 'cronus-system',
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  useNewUrlParser: true,
};

mongoose.connect('mongodb://10.252.220.52:27017', mongooseOptions)
  .then(() => {
    console.log('Connected to database!');
  })
  .catch(() => {
    console.log('Database connection failed!');
  });

require('./config/passport');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(passport.initialize());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});


app.use('/api/members', membersRoutes);
app.use('/api/teams', teamsRoute);
app.use('/api/user', userRoutes);
app.use('/api/ip' , ipRoutes);

module.exports = app;
